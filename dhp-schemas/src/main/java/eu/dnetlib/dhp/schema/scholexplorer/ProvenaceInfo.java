
package eu.dnetlib.dhp.schema.scholexplorer;

import java.io.Serializable;

public class ProvenaceInfo implements Serializable {

	private String id;

	private String name;

	private String completionStatus;

	private String collectionMode = "collected";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	public String getCollectionMode() {
		return collectionMode;
	}

	public void setCollectionMode(String collectionMode) {
		this.collectionMode = collectionMode;
	}
}
