
package eu.dnetlib.dhp.provision.scholix.summary;

import java.io.Serializable;

public enum Typology implements Serializable {
	dataset, publication, unknown
}
